/**
 * @file
 * CKEditor Read more functionality.
 */

(function ($) 
{  'use strict';
  Drupal.behaviors.ckeditorReadmore = {
    attach: function (context, settings) {
      var $ckeditorReadmore = $('.ckeditor-readmore');
      if ($ckeditorReadmore.length > 0) {
        $ckeditorReadmore
          .once()
          .wrap('<div class="ckeditor-readmore-wrapper"></div>')
          .parent()
          .prepend('<button class="ckeditor-readmore-btn">Read more</button>');
          
          $( document ).ready(function() {       
            $(".ckeditor-readmore").html(function (i, html) {
            return html.replace(/&nbsp;/, '');           
          });

          $(".ckeditor-readmore").each(function(){
            var arr = $(this).text();
             if(arr==''){
              $(this).parent().find('.ckeditor-readmore-btn').hide();
             }
            });    
          });

          $('body').once('ckeditorReadmoreToggleEvent').on('click', '.ckeditor-readmore-btn', function (ev) {
            $(this).next().slideToggle();
            $(this).parent().append('<button class="readless">Read Less</button>'); 
            $(this).css("display", "none"); 
          }); 
          
          $("body").on('click', '.readless', function (ev) {
            $(this).closest('div').find('.ckeditor-readmore').slideUp();
            $(this).parent().find('.ckeditor-readmore-btn').css("display", "block");
            $(this).css("display", "none");

          });
      }
    }
  };
})(jQuery);