CKEditor Read More


INTRODUCTION
-------------------
This module adds a new button to CKEditor which allows users to hide selected
content and only show it on "Read more" button click and hide content when click "Read less" button.


REQUIREMENTS
-------------------
CKEditor Module (Core)


INSTALLATION
-------------------
1.  Enable the module
2.  Enable "CKEditor Read More" plugin in CKEditor profile settings
    (admin/config/content/ckeditor).
3.  Drag and drop the Read More button to the CKEditor toolbar.


CONFIGURATION
-------------------
The module has no menu or modifiable settings. There is no configuration.
